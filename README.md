# get_next_line

get_next_line (GNL) reads from a file descriptor and returns 1 when it has successfully found a newline character. If run in a loop, it can print an entire string of text and return 0 once it reaches the EOF.